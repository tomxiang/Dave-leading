## Dave

Dave属于node一键生成curd代码的框架，便捷开发，教您不需要写代码也能弄接口

https://gitee.com/zzf0529/Dave


# app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
